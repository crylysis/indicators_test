package service

import (
	"context"
	"sync"

	"github.com/google/uuid"
	"github.com/robfig/cron/v3"
	"github.com/sirupsen/logrus"
)

type Scheduler struct {
	mx       sync.RWMutex
	workers  map[string]Worker
	sheduler *cron.Cron
	running  bool
}

func idGenerate() string {
	return uuid.NewString()
}

func InitScheduler() *Scheduler {
	return &Scheduler{
		workers: make(map[string]Worker, 10),
	}
}

func (s *Scheduler) AddWorker(ctx context.Context, worker Worker) (id string, err error) {
	s.mx.Lock()
	defer s.mx.Unlock()

	var idSchduler cron.EntryID
	id = idGenerate()
	s.workers[id] = worker

	if s.running {
		idSchduler, err = s.sheduler.AddFunc(worker.Spec(), func() {
			if err := worker.Do(ctx); err != nil {
				name, _ := worker.Status()
				logrus.Errorf("[scheduler worker %s] %v", name, err)
			}
		})
		if err != nil {
			return
		}

		worker.SetID(int(idSchduler))
	}

	return
}

func (s *Scheduler) DelWorker(id string) (ok bool) {
	s.mx.Lock()
	defer s.mx.Unlock()

	var worker Worker

	if worker, ok = s.workers[id]; !ok {
		return false
	} else {
		s.sheduler.Remove(cron.EntryID(worker.GetID()))
	}

	delete(s.workers, id)
	return
}

func (s *Scheduler) Run(ctx context.Context) (err error) {
	if len(s.workers) == 0 {
		return
	}

	s.sheduler = cron.New()

	for _, worker := range s.workers {
		id, err := s.sheduler.AddFunc(worker.Spec(), func() {
			worker.Do(ctx)
		})
		if err != nil {
			return err
		}
		worker.SetID(int(id))
	}

	go s.stopScheduler(ctx)
	s.sheduler.Run()
	return
}

func (s *Scheduler) stopScheduler(ctx context.Context) (err error) {
	<-ctx.Done()
	logrus.Warn("[stopScheduler] Stoping runner")
	s.sheduler.Stop()
	return nil
}
