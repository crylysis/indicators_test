package memstore

type UnitSaver interface {
	Save(...any) error
}

type UnitGetter interface {
	Get() []any
}

type UnitProcessor interface {
	UnitSaver
	UnitGetter
}
