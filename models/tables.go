package models

type SortType string

const (
	Sort_DESC SortType = "DESC"
	Sort_ASC  SortType = "ASC"
)

const (
	TableCandles = "candle_lines"
	TablePairs   = "pairs"
)
