package config

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/sirupsen/logrus"
)

const CONFIG_APP_PATH = "CONFIG_APP"

func ReadConfig(config interface{}) error {
	fileName := os.Getenv(CONFIG_APP_PATH)
	if fileName == "" {
		fileName = "./config.json"
		logrus.Warnf("[ReadConfig] Filename is empty. Use default path %s", fileName)
	}

	data, err := os.ReadFile(fileName)
	if err != nil {
		return fmt.Errorf("[ReadConfig] failed read config file %s %w", fileName, err)
	}

	if err := json.Unmarshal(data, config); err != nil {
		return fmt.Errorf("[ReadConfig] failed parse config file %w", err)
	}

	return nil
}
