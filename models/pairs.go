package models

import (
	"fmt"
	"strings"

	sq "github.com/Masterminds/squirrel"
)

type PairType string

const (
	BTC_USDT PairType = "BTC/USDT"
)

type Pair struct {
	ID         string
	Name       string
	ExchangeID string
}

func (p *Pair) ToString() string {
	builder := strings.Builder{}

	if p.ID != "" {
		builder.WriteString(" ID." + p.ID)
	}
	if p.Name != "" {
		builder.WriteString(" Name." + p.Name)
	}
	if p.ExchangeID != "" {
		builder.WriteString(" ExchangeID." + p.ExchangeID)
	}

	return builder.String()
}

type PairFilter struct {
	IDs        []string
	Name       PairType
	ExchangeID string

	Limit, Offset uint64
}

func (c *PairFilter) columns() []string {
	return []string{
		"id",
		"name",
		"exchange_id",
	}
}

func (c *PairFilter) BuildQuery() (string, []interface{}, error) {
	if c == nil {
		return "", nil, fmt.Errorf("object is nil")
	}

	builder := sq.StatementBuilder.PlaceholderFormat(sq.Dollar).Select(c.columns()...).From(TablePairs)
	eq := sq.Eq{}

	if len(c.IDs) > 0 {
		eq["id"] = c.IDs
	}

	if len(c.Name) > 0 {
		eq["name"] = c.Name
	}

	if len(c.ExchangeID) > 0 {
		eq["exchange_id"] = c.Name
	}

	if len(eq) > 0 {
		builder.Where(eq)
	}

	if c.Limit > 0 {
		builder.Limit(c.Limit)
	}

	if c.Offset > 0 {
		builder.Offset(c.Offset)
	}

	sql, args, er := builder.ToSql()
	if er != nil {
		return "", nil, fmt.Errorf("[BuildQuery] failed build query")
	}

	return sql, args, nil
}
