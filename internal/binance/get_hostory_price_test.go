package binance

import (
	"indicator/models"
	"log"
	"testing"
	"time"
)

func TestGetKlineData(t *testing.T) {
	timeStart, err := time.Parse(time.RFC3339, "2022-01-01T00:00:00Z")
	if err != nil {
		t.Error(err)
		return
	}

	timeEnd := timeStart.Add(time.Minute * 5000)

	data, err := GetKlineData(timeStart.Unix(), timeEnd.Unix(), models.FiveMinuts, "id pair test")
	if err != nil {
		log.Panic(err)
	}

	log.Println("Get list length", len(data.List))
	log.Println("First element - ",
		data.List[0].TSOpen,
		data.List[0].TSClose,
		data.List[0].OpenPrice,
		data.List[0].ClosePrice,
		data.List[0].MinPrice,
		data.List[0].MaxPrice,
		data.List[0].NumberOfTrades,
		data.List[0].Quate,
		data.List[0].Volume,
		data.List[0].TakerBuyBase,
		data.List[0].TakerBuyQuote,
	)
	log.Println("Second element - ",
		data.List[0].TSOpen,
		data.List[0].TSClose,
		data.List[0].OpenPrice,
		data.List[0].ClosePrice,
		data.List[0].MinPrice,
		data.List[0].MaxPrice,
		data.List[0].NumberOfTrades,
		data.List[0].Quate,
		data.List[0].Volume,
		data.List[0].TakerBuyBase,
		data.List[0].TakerBuyQuote,
	)
}

func TestTimeAdd(t *testing.T) {
	var timeEnd time.Time

	timeStart, err := time.Parse(time.RFC3339, "2022-01-01T00:00:00Z")
	if err != nil {
		t.Error(err)
		return
	}

	log.Println(timeStart)

	for {
		timeEnd = timeStart.Add(time.Minute * 5000)

		if timeEnd.Unix() <= time.Now().Unix() {
			log.Println(timeEnd)
			timeStart = timeEnd

		} else {

			break
		}
	}

	log.Println(timeEnd)
}
