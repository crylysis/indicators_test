package test

import (
	"context"
	"indicator/models"
	"sync"
	"testing"
	"time"

	"github.com/sirupsen/logrus"
)

func TestRSIParser(t *testing.T) {
	testData := []float64{54.8, 56.8, 57.85, 59.85, 60.57, 61.1, 62.17, 60.6, 62.35, 62.15, 62.35, 61.45, 62.8, 61.37, 62.5, 62.57, 60.8, 59.37, 60.35, 62.35, 62.17, 62.55, 64.55, 64.37, 65.3, 64.42, 62.9, 61.6, 62.05, 60.05, 59.7, 60.9, 60.25, 58.27, 58.7, 57.72, 58.1, 58.2}

	// init queue
	rsi14Queue := models.NewCandlePriceQueue(14)

	// init channel
	ctxWriter, cancelWriter := context.WithCancel(context.Background())
	ctxReader, cancelReader := context.WithCancel(context.Background())
	priceChan14 := make(chan *models.CandlePriceUnit)
	rsiChan14 := make(chan *models.RSIUnit)

	// Run queue workers
	rsi14Queue.RunPriceWorker(ctxWriter, priceChan14, rsiChan14)

	wg := sync.WaitGroup{}
	rsiList14 := make([]*models.RSIUnit, 14, len(testData))

	wg.Add(1)
	go func() {
		defer wg.Done()
		for {
			select {
			case rsi, ok := <-rsiChan14:
				if !ok {
					return
				}
				rsiList14 = append(rsiList14, rsi)

			case <-ctxReader.Done():
				if ctxReader.Err() != nil {
					logrus.Warn(ctxReader.Err())
					return
				}
			}
		}
	}()

	// Run service
	for idx, price := range testData {
		if idx == len(testData) {
			break
		}

		priceChan14 <- &models.CandlePriceUnit{
			TSOpen:    time.Now(),
			OpenPrice: price,
		}
	}

	close(priceChan14)
	cancelWriter()
	rsi14Queue.Wait()
	cancelReader()
	wg.Wait()
	close(rsiChan14)

	testTable := map[int]struct {
		Price  float64
		Result float64
	}{
		0: struct{ Price, Result float64 }{
			Price:  54.8,
			Result: 0.0,
		},
		1: struct{ Price, Result float64 }{
			Price:  56.8,
			Result: 0.0,
		},
		2: struct{ Price, Result float64 }{
			Price:  57.85,
			Result: 0.0,
		},
		3: struct{ Price, Result float64 }{
			Price:  59.85,
			Result: 0.0,
		},
		4: struct{ Price, Result float64 }{
			Price:  60.57,
			Result: 0.0,
		},
		5: struct{ Price, Result float64 }{
			Price:  61.1,
			Result: 0.0,
		},
		6: struct{ Price, Result float64 }{
			Price:  62.17,
			Result: 0.0,
		},
		7: struct{ Price, Result float64 }{
			Price:  60.6,
			Result: 0.0,
		},
		8: struct{ Price, Result float64 }{
			Price:  62.35,
			Result: 0.0,
		},
		9: struct{ Price, Result float64 }{
			Price:  62.15,
			Result: 0.0,
		},
		10: struct{ Price, Result float64 }{
			Price:  62.35,
			Result: 0.0,
		},
		11: struct{ Price, Result float64 }{
			Price:  61.45,
			Result: 0.0,
		},
		12: struct{ Price, Result float64 }{
			Price:  62.8,
			Result: 0.0,
		},
		13: struct{ Price, Result float64 }{
			Price:  61.37,
			Result: 0.0,
		},
		14: struct{ Price, Result float64 }{
			Price:  62.5,
			Result: 74.23,
		},
		15: struct{ Price, Result float64 }{
			Price:  62.57,
			Result: 74.55,
		},
		16: struct{ Price, Result float64 }{
			Price:  60.8,
			Result: 65.75,
		},
		17: struct{ Price, Result float64 }{
			Price:  59.37,
			Result: 59.68,
		},
		18: struct{ Price, Result float64 }{
			Price:  60.35,
			Result: 61.98,
		},
		19: struct{ Price, Result float64 }{
			Price:  62.35,
			Result: 66.44,
		},
		20: struct{ Price, Result float64 }{
			Price:  62.17,
			Result: 65.75,
		},
		21: struct{ Price, Result float64 }{
			Price:  62.55,
			Result: 67.00,
		},
		22: struct{ Price, Result float64 }{
			Price:  64.55,
			Result: 70.76,
		},
		23: struct{ Price, Result float64 }{
			Price:  64.37,
			Result: 69.79,
		},
		24: struct{ Price, Result float64 }{
			Price:  65.3,
			Result: 71.43,
		},
		25: struct{ Price, Result float64 }{
			Price:  64.42,
			Result: 67.32,
		},
		26: struct{ Price, Result float64 }{
			Price:  62.9,
			Result: 60.78,
		},
		27: struct{ Price, Result float64 }{
			Price:  61.6,
			Result: 55.56,
		},
		28: struct{ Price, Result float64 }{
			Price:  62.05,
			Result: 56.71,
		},
		29: struct{ Price, Result float64 }{
			Price:  60.05,
			Result: 49.49,
		},
	}

	for idx, rsi := range rsiList14 {
		if rsi == nil {
			continue
		}

		if idx > 29 {
			break
		}

		test, ok := testTable[idx]
		if !ok {
			t.Error("Bad test data")
			return
		}

		if rsi.RSI != test.Result {
			t.Errorf("Invalid RSI for price %f\nHave %f\nWont %f", testData[idx], rsi.RSI, test.Result)
			return
		}
	}

}
