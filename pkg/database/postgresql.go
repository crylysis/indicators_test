package database

import (
	"context"
	"errors"
	"fmt"

	pgxp "github.com/jackc/pgx/v4/pgxpool"
)

var (
	ErrConfigData = errors.New("")
	ErrConnect    = func(err error) error { return fmt.Errorf("failed connect to database:\n %v", err) }
)

func NewConnect(cfg *Config) (*pgxp.Pool, error) {
	pgConfigConnect, err := pgxp.ParseConfig(fmt.Sprintf(
		"postgres://%s:%s@%s/%s?sslmode=disable&pool_max_conns=%s",
		cfg.User, cfg.Password, cfg.Host, cfg.Database, cfg.Pool))
	if err != nil {
		return nil, ErrConfigData
	}

	dbPool, err := pgxp.ConnectConfig(context.Background(), pgConfigConnect)
	if err != nil {
		return nil, ErrConnect(err)
	}

	return dbPool, nil
}
