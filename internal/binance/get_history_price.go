package binance

import (
	"encoding/json"
	"fmt"
	"indicator/models"
	"io"
	"log"
	"net/http"
	"strconv"
	"time"
)

func GetKlineData(timeStart, timeEnd int64, interval models.IntervalType, idPair string) (*models.CandlePriceList, error) {
	klineList := make([]*models.CandlePriceUnit, 0)
	// body := &bytes.Buffer{}

	var (
		min, max = 1000000.0, 0.0
	)

	url := fmt.Sprintf("https://api1.binance.com/api/v3/klines?symbol=BTCUSDT&limit=1000&interval=%s&startTime=%d000&endTime=%d000", interval, timeStart, timeEnd)
	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return nil, err
	}

	if response.StatusCode != http.StatusOK {
		data, err := io.ReadAll(response.Body)
		if err != nil {
			data = nil
		}
		return nil, fmt.Errorf("fucking error %d with body %v", response.StatusCode, string(data))
	}

	var object interface{}

	data, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(data, &object)
	if err != nil {
		return nil, err
	}

	listRawItem, ok := object.([]interface{})
	if !ok {
		log.Panic("Invalid transform type")
	}

	for _, rawItem := range listRawItem {
		value := &models.CandlePriceUnit{}

		newItem, ok := rawItem.([]interface{})
		if !ok {
			continue
		}

		value.PairID = idPair

		// Time open
		if i, ok := newItem[0].(float64); ok {
			value.TSOpen = time.Unix(int64(i)/1000, 0)
		}

		// Time close
		if i, ok := newItem[6].(float64); ok {
			value.TSClose = time.Unix(int64(i)/1000, 0)
		}

		// Open price
		if i, ok := newItem[1].(string); ok {
			val, err := strconv.ParseFloat(i, 64)
			if err != nil {
				continue
			}

			value.OpenPrice = val
		}

		// Close price
		if i, ok := newItem[4].(string); ok {
			val, err := strconv.ParseFloat(i, 64)
			if err != nil {
				continue
			}

			value.ClosePrice = val
		}

		// Min price
		if i, ok := newItem[3].(string); ok {
			val, err := strconv.ParseFloat(i, 64)
			if err != nil {
				continue
			}

			if val < min {
				min = val
			}

			value.MinPrice = val
		}

		// Max price
		if i, ok := newItem[2].(string); ok {
			val, err := strconv.ParseFloat(i, 64)
			if err != nil {
				continue
			}

			if val > max {
				max = val
			}

			value.MaxPrice = val
		}

		// Volume
		if i, ok := newItem[5].(string); ok {
			value.Volume = i
		}

		// Quote asset volume
		if i, ok := newItem[7].(string); ok {
			value.Quate = i
		}

		// Number of trades
		if i, ok := newItem[8].(int64); ok {
			value.NumberOfTrades = i
		}

		// Taker buy base asset volume
		if i, ok := newItem[9].(string); ok {
			value.TakerBuyBase = i
		}

		// Taker buy quote asset volume
		if i, ok := newItem[10].(string); ok {
			value.TakerBuyQuote = i
		}

		klineList = append(klineList, value)
	}

	return &models.CandlePriceList{
		List:     klineList,
		MaxPrice: max,
		MinPrice: min,
	}, nil
}
