package models_test

import (
	"indicator/models"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestPairFilter(t *testing.T) {
	tableTest := []struct {
		Filter models.PairFilter
		SQL    string
		ARGs   []any
	}{}

	for _, variant := range tableTest {
		sql, args, err := variant.Filter.BuildQuery()
		if err != nil {
			t.Error(err)
			return
		}

		require.Equal(t, variant.SQL, sql)
		require.Equal(t, variant.ARGs, args)
	}
}
