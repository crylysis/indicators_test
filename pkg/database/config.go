package database

type Config struct {
	Host     string `json:"host"`     // hostname
	User     string `json:"username"` // username for access database
	Password string `json:"password"` // password for access database
	Database string `json:"database"` // schem name for connect database
	Pool     string `json:"max_pool"` // max size pool connection
}
