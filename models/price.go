package models

import (
	"context"
	"fmt"
	"math"
	"strconv"
	"strings"
	"sync"
	"time"

	sq "github.com/Masterminds/squirrel"
	"github.com/sirupsen/logrus"
)

// 1499040000000,      // Kline open time
// "0.01634790",       // Open price
// "0.80000000",       // High price
// "0.01575800",       // Low price
// "0.01577100",       // Close price
// "148976.11427815",  // Volume
// 1499644799999,      // Kline Close time
// "2434.19055334",    // Quote asset volume
// 308,                // Number of trades
// "1756.87402397",    // Taker buy base asset volume
// "28.46694368",      // Taker buy quote asset volume
// "0"                 // Unused field, ignore.

type CandlePriceFilter struct {
	IDs    []string
	PairID string

	Sort          SortType
	Limit, Offset uint64
}

func (CandlePriceFilter) columns() []string {
	return []string{
		"id",
		"pair_id",
		"ts_open",
		"ts_close",
		"price_open",
		"price_close",
		"price_min",
		"price_max",
		"volumes",
		"quate",
		"number_of_trades",
		"taker_buy_base",
		"taker_buy_quote",
	}
}

func (c *CandlePriceFilter) BuildQuery() (string, []interface{}, error) {
	if c == nil {
		return "", nil, fmt.Errorf("object is nil")
	}

	builder := sq.StatementBuilder.Select(c.columns()...).PlaceholderFormat(sq.Dollar).From(TableCandles)
	eq := sq.Eq{}

	if len(c.IDs) > 0 {
		eq["id"] = c.IDs
	}

	if len(c.PairID) > 0 {
		eq["pair_id"] = c.PairID
	}

	if len(eq) > 0 {
		builder = builder.Where(eq)
	}

	if len(c.Sort) > 0 {
		builder = builder.OrderByClause("ts_close " + string(c.Sort))
	}

	if c.Limit > 0 {
		builder = builder.Limit(c.Limit)
	}

	if c.Offset > 0 {
		builder = builder.Offset(c.Offset)
	}

	return builder.ToSql()
}

type CandlePriceList struct {
	List     []*CandlePriceUnit
	MaxPrice float64
	MinPrice float64
}

type CandlePriceUnit struct {
	ID             string
	PairID         string
	TSOpen         time.Time
	TSClose        time.Time
	OpenPrice      float64
	ClosePrice     float64
	MinPrice       float64
	MaxPrice       float64
	Volume         string
	Quate          string
	NumberOfTrades int64
	TakerBuyBase   string
	TakerBuyQuote  string
}

func (c *CandlePriceUnit) ToString() string {
	builder := strings.Builder{}
	if c.ID != "" {
		builder.WriteString(" id." + c.ID)
	}
	builder.WriteString(" time_open." + c.TSOpen.Format(time.RFC3339))
	builder.WriteString(" time_close." + c.TSClose.Format(time.RFC3339))
	builder.WriteString(" price_open." + strconv.FormatFloat(c.OpenPrice, 'f', 10, 64))
	builder.WriteString(" price_close." + strconv.FormatFloat(c.ClosePrice, 'f', 10, 64))
	builder.WriteString(" price_min." + strconv.FormatFloat(c.MinPrice, 'f', 10, 64))
	builder.WriteString(" price_max." + strconv.FormatFloat(c.MaxPrice, 'f', 10, 64))
	builder.WriteString(" volume." + c.Volume)
	builder.WriteString(" quate." + c.Quate)
	builder.WriteString(" number_of_trades." + strconv.FormatInt(c.NumberOfTrades, 10))
	builder.WriteString(" taker_buy_base." + c.TakerBuyBase)
	builder.WriteString(" taker_buy_quate." + c.TakerBuyQuote)

	return builder.String()
}

type CandlePriceQueueUnit struct {
	Current  *CandlePriceUnit
	Next     *CandlePriceQueueUnit
	Previous *CandlePriceQueueUnit
}

type CandlePriceQueue struct {
	length    int32
	current   int32
	rsiSize   int32
	price     *CandlePriceQueueUnit
	lastPrice *CandlePriceQueueUnit
	lastUc    float64
	lastDc    float64
	firstWSM  bool
	wg        sync.WaitGroup
}

func NewCandlePriceQueue(rsiSize int32) *CandlePriceQueue {
	return &CandlePriceQueue{
		wg:      sync.WaitGroup{},
		rsiSize: rsiSize,
		length:  rsiSize * 3,
		price:   nil,
		current: 0,
	}
}

func (pq *CandlePriceQueue) addPrice(price *CandlePriceUnit) {
	// Create first price in queue
	if pq.price == nil {
		pq.price = &CandlePriceQueueUnit{
			Current:  price,
			Next:     nil,
			Previous: nil,
		}
		return
	}

	// Add new element
	bufPrice := *pq.price
	pq.price = &CandlePriceQueueUnit{
		Current: price,
		Next:    &bufPrice,
	}
	bufPrice.Previous = pq.price

	// Check limit reached
	if pq.current == pq.length {
		pq.delLast()
	} else {
		pq.current++
	}
}

func (pq *CandlePriceQueue) delLast() {
	if pq == nil || pq.lastPrice == nil {
		return
	}

	// Delete last element and shift point "lastPrice" on previous element
	bufPrice := *pq.lastPrice.Previous
	bufPrice.Next = nil
	pq.lastPrice = &bufPrice
}

func roundTo(n float64, decimals uint32) float64 {
	return math.Round(n*math.Pow(10, float64(decimals))) / math.Pow(10, float64(decimals))
}

func (pq *CandlePriceQueue) rsiSummator() (rsi float64) {
	if pq == nil {
		return 0.0
	}

	var rs, uc, dc float64

	// Uc - up difference counter
	// Dc - down difference counter

	if !pq.firstWSM {
		pq.firstWSM = true
		uc, dc, _ = recursiveCalculatePriceDifference(pq.price, pq.rsiSize-1)
		if dc == 0 {
			dc = 100.0
		}

		uc = uc / float64(pq.rsiSize)
		dc = dc / float64(pq.rsiSize)

	} else {
		uc, dc, _ = recursiveCalculatePriceDifference(pq.price, 0)

		uc = (pq.lastUc*float64(pq.rsiSize-1) + uc) / float64(pq.rsiSize)
		dc = (pq.lastDc*float64(pq.rsiSize-1) + dc) / float64(pq.rsiSize)
		uc = roundTo(uc, 2)
		dc = roundTo(dc, 2)
	}

	rs = roundTo(uc/dc, 2)

	pq.lastUc = uc
	pq.lastDc = dc

	rsi = 100 - (100 / (1 + rs))
	rsi = math.Round(rsi*100) / 100

	return rsi
}

func recursiveCalculatePriceDifference(next *CandlePriceQueueUnit, count int32) (uc float64, dc float64, defference float64) {
	if count > 0 && next != nil {
		uc, dc, _ = recursiveCalculatePriceDifference(next.Next, count-1)
	}

	currentPrice := next.Current
	nextPrice := next.Next.Current

	defference = (currentPrice.OpenPrice - nextPrice.OpenPrice)
	defference = math.Round(defference*100) / 100

	if defference > 0 {
		uc += defference
		uc = math.Round(uc*100) / 100
	}
	if defference < 0 {
		dc += math.Abs(defference)
		dc = math.Round(dc*100) / 100
	}

	return
}

func (pq *CandlePriceQueue) RunPriceWorker(ctx context.Context, priceChan <-chan *CandlePriceUnit, rsiChan chan<- *RSIUnit) {
	if pq == nil {
		return
	}

	go func() {

		defer close(rsiChan)

		for {
			select {
			// Interrupt worker
			case <-ctx.Done():
				logrus.Infof("RSI worker %d get stop signal", pq.rsiSize)
				return
			default:
			}

			select {
			// Price constructor
			case price, ok := <-priceChan:
				if !ok {
					logrus.Infof("RSI worker %d cant read from channel", pq.rsiSize)
					return
				}

				// Add new price
				pq.addPrice(price)

				// Sum RSI on now moment
				if pq.current >= pq.rsiSize {
					rsi := pq.rsiSummator()
					rsiChan <- &RSIUnit{
						CandleId: price.ID,
						RSI:      rsi,
					}
				}
			// Interrupt worker
			case <-ctx.Done():
				logrus.Infof("RSI worker %d get stop signal", pq.rsiSize)
				return
			default:
			}
		}

	}()
}
