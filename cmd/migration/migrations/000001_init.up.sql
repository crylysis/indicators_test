-- Active: 1687632074929@@127.0.0.1@5433@cryex
-- Exchanges

CREATE TABLE "exchanges" (
    "id" UUID NOT NULL PRIMARY KEY DEFAULT gen_random_uuid(),
    "_trigger" BOOLEAN DEFAULT FALSE,
    "ts_create" TIMESTAMPTZ NOT NULL,
    "ts_modify" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    "name" VARCHAR(80)  NOT NULL
);

COMMENT ON COLUMN "exchanges"."id" IS 'Идентификатор записи (пользователя)';
COMMENT ON COLUMN "exchanges"."_trigger" IS 'Поле для определение срабатывания триггера';
COMMENT ON COLUMN "exchanges"."ts_create" IS 'Время создания записи';
COMMENT ON COLUMN "exchanges"."ts_modify" IS 'Время обновление записи';
COMMENT ON COLUMN "exchanges"."name" IS 'Наименование биржи';

-- Pairs

CREATE TABLE "pairs" (
    "id" UUID NOT NULL PRIMARY KEY DEFAULT gen_random_uuid(),
    "_trigger" BOOLEAN DEFAULT FALSE,
    "ts_create" TIMESTAMPTZ NOT NULL,
    "ts_modify" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    "name" TEXT NOT NULL,
    "exchange_id" UUID REFERENCES "exchanges" ("id") NOT NULL
);

-- COMMENT ON COLUMN "pairs"."id" IS 'Идентификатор записи (пользователя)';
-- COMMENT ON COLUMN "pairs"."_trigger" IS 'Поле для определение срабатывания триггера';
-- COMMENT ON COLUMN "pairs"."ts_create" IS 'Время создания записи';
-- COMMENT ON COLUMN "pairs"."ts_modify" IS 'Время обновление записи';
-- COMMENT ON COLUMN "pairs"."name" IS 'Наименование пары';
-- COMMENT ON COLUMN "pairs"."exchange" IS 'Биржа';

CREATE TABLE "candle_lines" (
    "id" UUID NOT NULL PRIMARY KEY DEFAULT gen_random_uuid(),
    "_trigger" BOOLEAN DEFAULT FALSE,
    "ts_create" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    "ts_modify" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    "pair_id" UUID NOT NULL REFERENCES "pairs" ("id"),
    "ts_open" TIMESTAMPTZ NOT NULL,
    "ts_close" TIMESTAMPTZ NOT NULL,
    "price_open" NUMERIC(20,8) NOT NULL,
    "price_close" NUMERIC(20,8) NOT NULL,
    "price_min" NUMERIC(20,8) NOT NULL,
    "price_max" NUMERIC(20,8) NOT NULL,
    "volumes" TEXT NOT NULL DEFAULT '',
    "quate" TEXT NOT NULL DEFAULT '',
    "number_of_trades" BIGINT NOT NULL DEFAULT 0,
    "taker_buy_base" TEXT NOT NULL DEFAULT '',
    "taker_buy_quote" TEXT NOT NULL DEFAULT '',
    UNIQUE ("pair_id", "ts_open", "ts_close")
);

-- COMMENT ON COLUMN "pairs"."id" IS 'Идентификатор записи (пользователя)';
-- COMMENT ON COLUMN "pairs"."_trigger" IS 'Поле для определение срабатывания триггера';
-- COMMENT ON COLUMN "pairs"."ts_create" IS 'Время создания записи';
-- COMMENT ON COLUMN "pairs"."ts_modify" IS 'Время обновление записи';
-- COMMENT ON COLUMN "pairs"."pair_id" IS 'Идентификатор пары';
-- COMMENT ON COLUMN "pairs"."ts_open" IS 'Время отрытия';
-- COMMENT ON COLUMN "pairs"."ts_close" IS 'Время закрытия';
-- COMMENT ON COLUMN "pairs"."price_open" IS 'Электронная почта пользователя';
-- COMMENT ON COLUMN "pairs"."price_close" IS 'Электронная почта пользователя';