package service

import (
	"context"
	"sync/atomic"
)

type Worker interface {
	SetID(int)
	GetID() int

	Spec() string
	Status() (string, bool)
	Do(ctx context.Context) error
}

type SyncWorker struct {
	id   int64
	name string
	spec string
	job  func() error
}

func NewWorker(name, spec string, job func() error) Worker {
	return &SyncWorker{
		name: name,
		spec: spec,
		job:  job,
	}
}

func (sw *SyncWorker) SetID(id int) {
	atomic.StoreInt64(&sw.id, int64(id))
}

func (sw *SyncWorker) GetID() int {
	return int(atomic.LoadInt64(&sw.id))
}

func (sw *SyncWorker) Spec() string {
	return sw.spec
}

func (sw *SyncWorker) Status() (string, bool) {
	return sw.name, sw.id > 0
}

func (sw *SyncWorker) Do(ctx context.Context) error {
	return sw.job()
}
