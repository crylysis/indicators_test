package service

import (
	"context"
	"indicator/models"
	"sync"

	"github.com/sirupsen/logrus"
)

type BusService struct {
	wg  sync.WaitGroup
	out []chan []*models.CandlePriceUnit
	in  chan []*models.CandlePriceUnit
	ctx context.Context
}

func NewBusService(ctx context.Context, source chan []*models.CandlePriceUnit, subscriptions []chan []*models.CandlePriceUnit) *BusService {
	return &BusService{
		out: subscriptions,
		in:  source,
		ctx: ctx,
	}
}

func closeAll(outs []chan []*models.CandlePriceUnit) {
	for _, out := range outs {
		close(out)
	}
}

func (bs *BusService) Run() {
	bs.wg.Add(1)
	go func() {
		defer bs.wg.Done()
		bs.run()
	}()
}

func (bs *BusService) run() {
	for {

		select {
		case <-bs.ctx.Done():
			// closeAll(bs.out)
			logrus.Info("bus service get stop signal.")
			return

		case data, ok := <-bs.in:
			if !ok {
				// closeAll(bs.out)
				return
			}

			for _, out := range bs.out {
				out := out

				// TODO: review
				go func() {
					out <- data
				}()
			}
		}
	}
}

func (bs *BusService) Wait() {
	bs.wg.Wait()
	logrus.Debug("Die BUS service")
}
