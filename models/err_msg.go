package models

import "fmt"

var (
	ErrNoRowsInResult = fmt.Errorf("no rows in result")
)
