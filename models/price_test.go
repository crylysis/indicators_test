package models_test

import (
	"indicator/models"
	"log"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestCandlePriceFilter(t *testing.T) {
	tableTest := []struct {
		Name   string
		Filter models.CandlePriceFilter
		SQL    string
		ARGs   []any
	}{
		{
			Name:   "Empty",
			Filter: models.CandlePriceFilter{},
			SQL:    "SELECT id, pair_id, ts_open, ts_close, price_open, price_close, price_min, price_max, volumes, quate, number_of_trades, taker_buy_base, taker_buy_quote FROM candle_lines",
			ARGs:   nil,
		},
		{
			Name: "One ID",
			Filter: models.CandlePriceFilter{
				IDs: []string{"1"},
			},
			SQL:  "SELECT id, pair_id, ts_open, ts_close, price_open, price_close, price_min, price_max, volumes, quate, number_of_trades, taker_buy_base, taker_buy_quote FROM candle_lines WHERE id IN ($1)",
			ARGs: []any{"1"},
		},
		{
			Name: "Several ID",
			Filter: models.CandlePriceFilter{
				IDs: []string{"1", "2"},
			},
			SQL:  "SELECT id, pair_id, ts_open, ts_close, price_open, price_close, price_min, price_max, volumes, quate, number_of_trades, taker_buy_base, taker_buy_quote FROM candle_lines WHERE id IN ($1,$2)",
			ARGs: []any{"1", "2"},
		},
		{
			Name: "PairID",
			Filter: models.CandlePriceFilter{
				PairID: "1",
			},
			SQL:  "SELECT id, pair_id, ts_open, ts_close, price_open, price_close, price_min, price_max, volumes, quate, number_of_trades, taker_buy_base, taker_buy_quote FROM candle_lines WHERE pair_id = $1",
			ARGs: []any{"1"},
		},
		{
			Name: "OrderBy DESC",
			Filter: models.CandlePriceFilter{
				Sort: models.Sort_DESC,
			},
			SQL:  "SELECT id, pair_id, ts_open, ts_close, price_open, price_close, price_min, price_max, volumes, quate, number_of_trades, taker_buy_base, taker_buy_quote FROM candle_lines ORDER BY ts_close DESC",
			ARGs: nil,
		},
		{
			Name: "OrderBy ASC",
			Filter: models.CandlePriceFilter{
				Sort: models.Sort_ASC,
			},
			SQL:  "SELECT id, pair_id, ts_open, ts_close, price_open, price_close, price_min, price_max, volumes, quate, number_of_trades, taker_buy_base, taker_buy_quote FROM candle_lines ORDER BY ts_close ASC",
			ARGs: nil,
		},
		{
			Name: "Offset",
			Filter: models.CandlePriceFilter{
				Offset: 10,
			},
			SQL:  "SELECT id, pair_id, ts_open, ts_close, price_open, price_close, price_min, price_max, volumes, quate, number_of_trades, taker_buy_base, taker_buy_quote FROM candle_lines OFFSET 10",
			ARGs: nil,
		},
		{
			Name: "Offset And Limit",
			Filter: models.CandlePriceFilter{
				Limit:  10,
				Offset: 10,
			},
			SQL:  "SELECT id, pair_id, ts_open, ts_close, price_open, price_close, price_min, price_max, volumes, quate, number_of_trades, taker_buy_base, taker_buy_quote FROM candle_lines LIMIT 10 OFFSET 10",
			ARGs: nil,
		},
	}

	t.Parallel()

	for _, variant := range tableTest {
		variant := variant
		t.Run(variant.Name, func(t *testing.T) {
			sql, args, err := variant.Filter.BuildQuery()
			if err != nil {
				t.Error(err)
				return
			}

			log.Println(sql)
			log.Println(args...)

			require.Equal(t, variant.SQL, sql)
			require.Equal(t, len(variant.ARGs), len(args))
		})
	}
}
