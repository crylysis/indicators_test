package main

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"

	"indicator/pkg/config"
	"indicator/pkg/database"
)

type Config struct {
	DB database.Config `json:"database"`
}

func main() {
	var (
		db  *sql.DB
		err error
		// driver mDatabase.Driver
		m *migrate.Migrate
	)

	cfg := &Config{}
	err = config.ReadConfig(cfg)
	if err != nil {
		logrus.Panic(err)
	}

	time.Sleep(time.Second * 3)

	connStr := fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable", cfg.DB.User, cfg.DB.Password, cfg.DB.Host, cfg.DB.Database)
	log.Println("Connect to database ", connStr)

	for i := 0; true; i++ {
		db, err = sql.Open("postgres", connStr)
		if err != nil {
			logrus.Print(err)
			if i < 5 {
				time.Sleep(time.Second * 2)
				continue
			}
			logrus.Panic(err)
		}

		break
	}
	db.Close()

	m, err = migrate.New("file:///migrations", connStr)
	if err != nil {
		logrus.Panic(err)
	}

	if err = m.Up(); err != nil {
		logrus.Panic("Invalid up migration ", err)
	} // or m.Step(2) if you want to explicitly set the number of migrations to run

	logrus.Println("Success migrate")
}
