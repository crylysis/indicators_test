package database

import (
	"context"

	"github.com/jackc/pgconn"
	"github.com/jackc/pgx/v4"
	pgxp "github.com/jackc/pgx/v4/pgxpool"
	"github.com/sirupsen/logrus"
)

type Tx struct {
	db *pgxp.Pool
}

type Conn interface {
	Exec(ctx context.Context, sql string, arguments ...interface{}) (commandTag pgconn.CommandTag, err error)

	Query(ctx context.Context, sql string, args ...interface{}) (pgx.Rows, error)
	QueryRow(ctx context.Context, sql string, args ...interface{}) pgx.Row
	QueryFunc(ctx context.Context, sql string, args []interface{}, scans []interface{}, f func(pgx.QueryFuncRow) error) (pgconn.CommandTag, error)
}

type DBFunc func(Conn) error

func NewTx(db *pgxp.Pool) *Tx {
	return &Tx{db}
}

func (t *Tx) WithoutTx(ctx context.Context, ftx DBFunc) error {
	conn, err := t.db.Acquire(ctx)
	if err != nil {
		return err
	}

	defer conn.Release()

	if err := ftx(conn); err != nil {
		return err
	}

	return nil
}

func (t *Tx) WithTx(ctx context.Context, ftx DBFunc) error {
	var (
		err  error
		conn *pgxp.Conn
		tx   pgx.Tx
	)

	conn, err = t.db.Acquire(ctx)
	if err != nil {
		return err
	}

	defer conn.Release()

	tx, err = conn.Begin(ctx)
	if err != nil {
		return err
	}

	defer func() {
		if err = tx.Commit(ctx); err != nil {
			if err != pgx.ErrTxClosed {
				logrus.Error(err)
			}
		}
	}()

	if err := ftx(tx); err != nil {
		if rollErr := tx.Rollback(ctx); rollErr != nil {
			return err
		}
		return err
	}

	return nil
}
