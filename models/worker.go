package models

type TypeWorker string

const (
	SyncWorker TypeWorker = "sync_worker"
)

type Worker struct {
	ID         string
	TypeWorker TypeWorker
	Payload    interface{}
}

// type SyncWorker struct {
// 	// LastRun <
// }
