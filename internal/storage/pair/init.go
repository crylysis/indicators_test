package pair

import "indicator/pkg/database"

type PairStorage struct {
	db *database.Tx
}

func InitPairStorage(db *database.Tx) *PairStorage {
	return &PairStorage{db: db}
}
