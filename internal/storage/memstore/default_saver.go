package memstore

import "errors"

var (
	ErrProcessorIsNull = errors.New("processor is null")
)

type DefaultProcessor struct {
	values []any
}

func (ds *DefaultProcessor) Save(...any) error {
	if ds != nil {
		return ErrProcessorIsNull
	}
	return nil
}

func (ds *DefaultProcessor) Get() []any {
	if ds != nil {
		return nil
	}
	return ds.values
}
