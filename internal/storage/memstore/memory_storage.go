package memstore

import (
	"sync"

	"github.com/sirupsen/logrus"
)

type MemoryStorage struct {
	mx           sync.RWMutex
	sizeData     int
	unitStorages map[string]UnitProcessor
}

func NewMemoryStorage() *MemoryStorage {
	return &MemoryStorage{
		mx:           sync.RWMutex{},
		sizeData:     0,
		unitStorages: make(map[string]UnitProcessor, 200_000),
	}
}

func (m *MemoryStorage) AddProcessor(key string, saver UnitProcessor) {
	m.mx.Lock()
	defer m.mx.Unlock()

	_, ok := m.unitStorages[key]
	if !ok {
		m.unitStorages[key] = saver
	}
}

func (m *MemoryStorage) Save(key string, results ...any) {
	m.mx.Lock()
	defer m.mx.Unlock()

	st, ok := m.unitStorages[key]
	if !ok {
		st = &DefaultProcessor{}
	}

	st.Save(results)
	m.unitStorages[key] = st
}

func (m *MemoryStorage) ShowInfoAboutStateStorage() {
	m.mx.RLock()
	defer m.mx.RUnlock()

	for key, val := range m.unitStorages {
		logrus.Infof("key %s have %d elements", key, len(val.Get()))
	}
}
