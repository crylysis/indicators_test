package pair

import (
	"context"
	"fmt"
	"indicator/models"
	"indicator/pkg/database"

	"github.com/jackc/pgx/v4"
)

func (p *PairStorage) GetItem(ctx context.Context, filter *models.PairFilter) (pair *models.Pair, err error) {
	var (
		sql         string
		args        []interface{}
		execFunctin database.DBFunc
	)

	if sql, args, err = filter.BuildQuery(); err != nil {
		return nil, fmt.Errorf("[GetItem] filter.BuildQuery %w", err)
	}

	execFunctin = func(conn database.Conn) error {
		pair = &models.Pair{}
		err := conn.QueryRow(ctx, sql, args...).Scan(
			&pair.ID,
			&pair.Name,
			&pair.ExchangeID,
		)
		if err != nil {
			if err == pgx.ErrNoRows {
				return models.ErrNoRowsInResult
			}

			return fmt.Errorf("failed scan session %w", err)
		}
		return nil
	}

	if err = p.db.WithoutTx(ctx, execFunctin); err != nil {
		return nil, fmt.Errorf("[GetItem] u.db.WithoutTx %w", err)
	}

	return
}

func (p *PairStorage) GetList(ctx context.Context, filter *models.PairFilter) (pairs []*models.Pair, err error) {
	var (
		sql         string
		args        []interface{}
		execFunctin database.DBFunc
	)

	if sql, args, err = filter.BuildQuery(); err != nil {
		return nil, fmt.Errorf("[GetList] filter.BuildQuery")
	}

	execFunctin = func(conn database.Conn) error {
		rows, err := conn.Query(ctx, sql, args...)
		if err != nil {
			return fmt.Errorf("[GetList] get list candles %w", err)
		}

		pairs = make([]*models.Pair, 0, 100)
		for rows.Next() {
			candle := &models.Pair{}
			err := rows.Scan(
				&candle.ID,
				&candle.Name,
				&candle.ExchangeID,
			)
			if err != nil {
				if err == pgx.ErrNoRows {
					return models.ErrNoRowsInResult
				}

				return fmt.Errorf("failed scan session %w", err)
			}

			pairs = append(pairs, candle)
		}
		return nil
	}

	if err = p.db.WithoutTx(ctx, execFunctin); err != nil {
		return nil, fmt.Errorf("[GetList] u.db.WithoutTx")
	}

	return
}
