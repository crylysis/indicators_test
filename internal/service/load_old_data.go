package service

import (
	"context"
	"indicator/internal/storage"
	"indicator/models"

	"github.com/sirupsen/logrus"
)

func LoadOldData(pair storage.PairStorage, candle storage.CandlePriceStorage, data chan []*models.CandlePriceUnit, pairType models.PairType) error {
	logrus.Debug("Load old data")
	pairData, err := pair.GetItem(context.Background(), &models.PairFilter{
		Name: pairType,
	})
	if err != nil {
		return err
	}

	logrus.Debug("pairData - ", pairData.ToString())

	candleData, err := candle.GetItem(context.Background(), &models.CandlePriceFilter{
		PairID: pairData.ID,
		Sort:   models.Sort_DESC,
		Limit:  1,
	})

	if err != nil {
		return nil
	}

	var step uint64 = 10000
	var current uint64 = 0

	for {
		candleList, err := candle.GetList(context.Background(), &models.CandlePriceFilter{
			PairID: pairData.ID,
			Sort:   models.Sort_ASC,
			Limit:  step,
			Offset: current,
		})

		if err != nil {
			return err
		}

		if len(candleList) == 0 {
			return nil
		}

		logrus.Debugf("Load on sync %d", len(candleList))
		data <- candleList

		if candleData.TSClose == candleList[len(candleList)-1].TSClose {
			return nil
		}

		current += step
	}
}
