package service

import (
	"context"
	"indicator/internal/binance"
	"indicator/internal/storage"
	"indicator/models"
	"time"

	"github.com/sirupsen/logrus"
)

type SyncWorkerService struct {
	pair        storage.PairStorage
	candle      storage.CandlePriceStorage
	timeForStap time.Duration
	timeForSync time.Duration
	data        chan []*models.CandlePriceUnit
}

func NewSyncWorkerService(
	pair storage.PairStorage,
	candle storage.CandlePriceStorage,
	timeFotStep time.Duration,
	timeFotSync time.Duration,
	data chan []*models.CandlePriceUnit) *SyncWorkerService {
	return &SyncWorkerService{
		pair:        pair,
		candle:      candle,
		timeForStap: timeFotStep,
		timeForSync: timeFotSync,
		data:        data,
	}
}

func (s *SyncWorkerService) SyncCandlePrice(ctx context.Context, pairType models.PairType) error {
	var timeEnd, timeStart time.Time

	logrus.Debug("Run sync")
	pairData, err := s.pair.GetItem(ctx, &models.PairFilter{
		Name: pairType,
	})
	if err != nil {
		return err
	}

	logrus.Debug("pairData - ", pairData.ToString())

	candleData, err := s.candle.GetItem(ctx, &models.CandlePriceFilter{
		PairID: pairData.ID,
		Sort:   models.Sort_DESC,
		Limit:  1,
	})

	if err != nil {
		if err != models.ErrNoRowsInResult {
			return err
		}

		timeStart, err = time.Parse(time.RFC3339, "2022-01-01T00:00:00Z")
		if err != nil {
			return err
		}
		logrus.Debug("Run sync from start 2022 year", timeStart.Format(time.RFC3339))

	} else {
		timeStart = candleData.TSClose
		logrus.Debug("Run sync from ", timeStart.Format(time.RFC3339))

	}

	timeStep := s.timeForSync

	for {
		select {
		case <-ctx.Done():
			return nil
		default:
		}

		timeEnd = timeStart.Add(timeStep)

		if timeEnd.Unix() <= time.Now().Add(timeStep).Unix() {
			logrus.Debugf("Time %s - %s", timeStart.Format(time.RFC3339), timeEnd.Format(time.RFC3339))

			data, err := binance.GetKlineData(timeStart.Unix(), timeEnd.Unix(), models.FiveMinuts, pairData.ID)
			if err != nil {
				return err
			}
			logrus.Debugf("Get items %d", len(data.List))

			if len(data.List) < 10 {
				timeStep = s.timeForStap
			}

			if err := s.candle.Create(ctx, data.List...); err != nil {
				return nil
			}

			s.data <- data.List

			timeStart = timeEnd.Add(time.Minute * 1)

		} else {
			break

		}

	}

	return nil
}
