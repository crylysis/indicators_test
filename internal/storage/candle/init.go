package candle

import "indicator/pkg/database"

type CandleStorage struct {
	db *database.Tx
}

func InitCandleStorage(db *database.Tx) *CandleStorage {
	return &CandleStorage{db: db}
}
