package service

import (
	"context"
	"indicator/internal/storage/memstore"
	"indicator/models"
	"strconv"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
)

type rsiWorker struct {
	Size   int
	PairID string
	In     chan *models.CandlePriceUnit
	Out    chan *models.RSIUnit
	Worker *models.CandlePriceQueue
}

type RSIService struct {
	wg         *sync.WaitGroup
	data       chan []*models.CandlePriceUnit
	rsiWorkers []*rsiWorker
	ctx        context.Context
}

type RSIResult struct {
	RSISize int
	Time    time.Time
	Data    *models.RSIUnit
}

type RSIUnitStorage struct {
	Value map[int]*RSIResult
}

var MemStorage *memstore.MemoryStorage

// Не понятно наименование InitInfo
// Вынести за пределы сервиса
// Изучить и реализовать как сущность.
type RSIUnitConfig struct {
	PairID string
	Data   chan []*models.CandlePriceUnit
	Size   []int
}

func NewRSIService(ctx context.Context, infoRSIs ...*RSIUnitConfig) *RSIService {
	workers := make([]*rsiWorker, 0)

	for _, info := range infoRSIs {

		for _, rsi := range info.Size {
			if rsi <= 0 {
				continue
			}

			logrus.Infof("Add RSI worker with size: %d", rsi)
			workers = append(workers, &rsiWorker{
				Size:   rsi,
				PairID: info.PairID,
				In:     make(chan *models.CandlePriceUnit, 1),
				Out:    make(chan *models.RSIUnit, 1),
				Worker: models.NewCandlePriceQueue(int32(rsi)),
			})
		}
	}

	MemStorage = memstore.NewMemoryStorage()

	logrus.Info("Init RSI service.")
	return &RSIService{
		wg:         &sync.WaitGroup{},
		rsiWorkers: workers,
		// data:       data,
		ctx: ctx,
	}
}

func (r *RSIService) ShowStatus() {
	if MemStorage != nil {
		MemStorage.ShowInfoAboutStateStorage()
	}
}

func (r *RSIService) Run() {
	for _, worker := range r.rsiWorkers {
		worker := worker

		r.wg.Add(1)
		go func() {
			defer func() {
				r.wg.Done()
				logrus.Infof("Die RSI wirker %d", worker.Size)
			}()

			worker.Worker.RunPriceWorker(r.ctx, worker.In, worker.Out)
			logrus.Infof("Run RSI worker %d", worker.Size)

			for data := range worker.Out {
				MemStorage.Save(
					strconv.Itoa(worker.Size),
					&RSIResult{
						RSISize: worker.Size,
						Time:    time.Now(),
						Data:    data,
					})
			}
		}()
	}

	r.wg.Add(1)
	go r.run()
}

func (r *RSIService) closeAllOuts() {
	for _, worker := range r.rsiWorkers {
		close(worker.Out)
	}
}

func (r *RSIService) run() {
	defer r.wg.Done()

	for {
		select {
		case <-r.ctx.Done():
			logrus.Info("RSI service get stop signal.")
			return
		case data, ok := <-r.data:
			if !ok {
				r.closeAllOuts()
				return
			}

			logrus.Debugf("RSI Service get %d elements on sync", len(data))
			for _, candlePriceUnit := range data {
				for _, worker := range r.rsiWorkers {
					worker.In <- candlePriceUnit
				}
			}
		}
	}
}

func (r *RSIService) Wait() {
	r.wg.Wait()
	logrus.Debug("Die RSI service")
}
