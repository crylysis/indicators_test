package test

import (
	"context"
	"indicator/internal/service"
	"indicator/internal/storage/candle"
	"indicator/internal/storage/pair"
	"indicator/models"
	"indicator/pkg/database"
	"indicator/utility/loggo"
	"os"
	"os/signal"
	"syscall"
	"testing"
	"time"

	pgxp "github.com/jackc/pgx/v4/pgxpool"

	"github.com/sirupsen/logrus"
)

type Config struct {
	Database    database.Config `json:"database"`
	SyncService SyncService     `json:"sync_service"`
}

type SyncService struct {
	TimeForStep int `json:"time_for_step"`
}

func TestGet(t *testing.T) {
	// INFO: Log trace level
	var (
		err     error
		dbPool  *pgxp.Pool
		txStore *database.Tx
	)

	// INFO: system signal
	stop := make(chan os.Signal, 1)
	signal.Notify(
		stop,
		syscall.SIGTERM,
		syscall.SIGINT,
	)

	loggo.InitCastomLogger(&logrus.JSONFormatter{TimestampFormat: "15:04:05 02/01/2006"}, logrus.TraceLevel, false, true)

	// INFO: Read config
	cfg := &Config{
		Database: database.Config{
			Host:     "localhost:5433",
			User:     "cryex",
			Password: "cryex",
			Database: "cryex",
			Pool:     "10",
		},
		SyncService: SyncService{
			TimeForStep: 10,
		},
	}

	// INFO: Init Database
	for i := 0; true; i++ {
		if dbPool, err = database.NewConnect(&cfg.Database); err != nil {
			logrus.Info(err)
			if i < 5 {
				time.Sleep(time.Second * 2)
				continue
			}
			t.Error(err)
		}
		break
	}

	txStore = database.NewTx(dbPool)

	syncService := service.NewSyncWorkerService(
		pair.InitPairStorage(txStore),
		candle.InitCandleStorage(txStore),
		time.Minute*time.Duration(cfg.SyncService.TimeForStep),
	)

	if err := syncService.SyncCandlePrice(context.Background(), models.BTC_USDT); err != nil {
		t.Error(err)
	}
}
