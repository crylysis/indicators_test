package models

import (
	"fmt"
	"strings"
)

type RSIUnit struct {
	ID       string
	CandleId string
	RSI      float64
}

func (t *RSIUnit) ToString() string {
	builder := strings.Builder{}

	if t.ID != "" {
		builder.WriteString(" id." + t.ID)
	}
	if t.CandleId != "" {
		builder.WriteString(" CandleId." + t.CandleId)
	}

	builder.WriteString(fmt.Sprintf(" RSI.%f", t.RSI))

	return builder.String()
}

type RSIFilter struct {
	CandleId string
}
