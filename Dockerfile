# Builer образ для сборки.
FROM golang:latest AS build
WORKDIR /go/src/service
COPY . .
RUN go build -o /indicator ./cmd/indicator/main.go && \
    cp ./config.json /config.json

# Service образ без артифактов.
FROM golang:latest
LABEL maintainer="mr.wgw@yandex.ru"
RUN mkdir /var/indicator
COPY --from=build /indicator /bin
COPY --from=build /config.json /var/indicator
CMD ["indicator"]
