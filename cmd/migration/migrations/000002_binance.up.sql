-- Active: 1687632074929@@127.0.0.1@5433@cryex
INSERT INTO "exchanges"("ts_create", "name")
VALUES (now(), 'binance');

INSERT INTO "pairs"("ts_create", "name", "exchange_id")
VALUES (now(), 'BTC/USDT', (
    SELECT "id" FROM "exchanges" WHERE name = 'binance'
));