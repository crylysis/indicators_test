package candle

import (
	"context"
	"fmt"
	"indicator/models"
	"indicator/pkg/database"

	"github.com/jackc/pgx/v4"
	"github.com/sirupsen/logrus"
)

func (c *CandleStorage) GetItem(ctx context.Context, filter *models.CandlePriceFilter) (candle *models.CandlePriceUnit, err error) {
	var (
		sql         string
		args        []interface{}
		execFunctin database.DBFunc
	)

	if sql, args, err = filter.BuildQuery(); err != nil {
		return nil, fmt.Errorf("[GetItem] filter.BuildQuery %w", err)
	}

	logrus.Debug("query - ", sql)
	logrus.Debug("args - ", args)

	execFunctin = func(conn database.Conn) error {
		candle = &models.CandlePriceUnit{}
		err := conn.QueryRow(ctx, sql, args...).Scan(
			&candle.ID,
			&candle.PairID,
			&candle.TSOpen,
			&candle.TSClose,
			&candle.OpenPrice,
			&candle.ClosePrice,
			&candle.MinPrice,
			&candle.MaxPrice,
			&candle.Volume,
			&candle.Quate,
			&candle.NumberOfTrades,
			&candle.TakerBuyBase,
			&candle.TakerBuyQuote,
		)

		if err != nil {
			if err != pgx.ErrNoRows {
				return fmt.Errorf("[execFunctin] failed scan candle %w", err)
			}

			return models.ErrNoRowsInResult
		}
		return nil
	}

	logrus.Debug("Query preparer ready. Exec! ")

	if err = c.db.WithoutTx(ctx, execFunctin); err != nil {
		if err != models.ErrNoRowsInResult {
			return nil, fmt.Errorf("[GetItem] u.db.WithoutTx %w", err)
		}
		return nil, err
	}

	return
}

func (u *CandleStorage) GetList(ctx context.Context, filter *models.CandlePriceFilter) (candles []*models.CandlePriceUnit, err error) {
	var (
		sql         string
		args        []interface{}
		execFunctin database.DBFunc
	)

	if sql, args, err = filter.BuildQuery(); err != nil {
		return nil, fmt.Errorf("[GetList] filter.BuildQuery")
	}

	execFunctin = func(conn database.Conn) error {
		rows, err := conn.Query(ctx, sql, args...)
		if err != nil {
			return fmt.Errorf("[GetList] get list candles %w", err)
		}

		candles = make([]*models.CandlePriceUnit, 0, 100)
		for rows.Next() {
			candle := &models.CandlePriceUnit{}
			err := rows.Scan(
				&candle.ID,
				&candle.PairID,
				&candle.TSOpen,
				&candle.TSClose,
				&candle.OpenPrice,
				&candle.ClosePrice,
				&candle.MinPrice,
				&candle.MaxPrice,
				&candle.Volume,
				&candle.Quate,
				&candle.NumberOfTrades,
				&candle.TakerBuyBase,
				&candle.TakerBuyQuote,
			)
			if err != nil {
				if err == pgx.ErrNoRows {
					return models.ErrNoRowsInResult
				}

				return fmt.Errorf("failed scan session %w", err)
			}

			candles = append(candles, candle)
		}
		return nil
	}

	if err = u.db.WithoutTx(ctx, execFunctin); err != nil {
		return nil, fmt.Errorf("[GetList] u.db.WithoutTx")
	}

	return
}
