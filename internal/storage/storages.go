package storage

import (
	"context"
	"indicator/models"
)

// User
type CandlePriceStorage interface {
	Create(context.Context, ...*models.CandlePriceUnit) error
	// Update(context.Context, []*models.PriceUnit) error
	// Delete(context.Context, []*models.PriceUnit) error

	GetItem(context.Context, *models.CandlePriceFilter) (*models.CandlePriceUnit, error)
	GetList(context.Context, *models.CandlePriceFilter) ([]*models.CandlePriceUnit, error)
}

type PairStorage interface {
	GetItem(context.Context, *models.PairFilter) (*models.Pair, error)
	GetList(context.Context, *models.PairFilter) ([]*models.Pair, error)
}

type WorkerStorage interface {
	Create()
	Update()
	GetItem()
}
