package main

import (
	"context"
	"indicator/internal/service"
	"indicator/internal/storage/candle"
	"indicator/internal/storage/pair"
	"indicator/models"
	"indicator/pkg/config"
	"indicator/pkg/database"
	"indicator/utility/loggo"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	pgxp "github.com/jackc/pgx/v4/pgxpool"

	"github.com/sirupsen/logrus"
)

// TODO: architecture work
// Проанализировать и обновить архитектуру проекта
// Задать правильную систему тестов.
// Унифицировать структуры и данные

const TraceLevel = "DEBUG_APP"

type Config struct {
	Database    database.Config `json:"database"`
	SyncService SyncService     `json:"sync_service"`
}

type SyncService struct {
	TimeForStep int `json:"time_for_step"`
	TimeForSync int `json:"time_for_sync"`
}

type ServiceRun interface {
	Run()
}

func main() {
	// INFO: Log trace level
	var (
		err     error
		level   logrus.Level
		dbPool  *pgxp.Pool
		txStore *database.Tx
	)

	ctx, cancel := context.WithCancel(context.Background())

	// INFO: system signal
	stop := make(chan os.Signal, 1)
	signal.Notify(
		stop,
		syscall.SIGTERM,
		syscall.SIGINT,
	)

	// INFO: Logger
	traceEnv := os.Getenv(TraceLevel)
	switch traceEnv {
	case "InfoLevel":
		{
			level = logrus.InfoLevel
		}
	case "TraceLevel":
		{
			level = logrus.TraceLevel
		}
	default:
		{
			logrus.Warn("Debug level invalid. Set default TraceLevel")
			level = logrus.TraceLevel
		}
	}

	loggo.InitCastomLogger(&logrus.JSONFormatter{TimestampFormat: "15:04:05 02/01/2006"}, level, false, true)

	// INFO: Read config
	cfg := &Config{}
	if err = config.ReadConfig(cfg); err != nil {
		logrus.Panic(err)
	}

	// INFO: Init Database
	for i := 0; true; i++ {
		if dbPool, err = database.NewConnect(&cfg.Database); err != nil {
			logrus.Print(err)
			if i < 5 {
				time.Sleep(time.Second * 2)
				continue
			}
			logrus.Panic(err)
		}
		break
	}

	txStore = database.NewTx(dbPool)

	// Analiz old data
	dataBTC := make(chan []*models.CandlePriceUnit, 1)
	rsiSubsribe := make(chan []*models.CandlePriceUnit, 1)

	busService := service.NewBusService(ctx, dataBTC, []chan []*models.CandlePriceUnit{
		rsiSubsribe,
	})

	// Init new RSI
	rsiService := service.NewRSIService(ctx, &service.RSIUnitConfig{
		PairID: "btc",
		Data:   rsiSubsribe,
		Size:   []int{25, 100},
	})

	rsiService.Run()
	busService.Run()

	syncService := service.NewSyncWorkerService(
		pair.InitPairStorage(txStore),
		candle.InitCandleStorage(txStore),
		time.Minute*time.Duration(cfg.SyncService.TimeForStep),
		time.Minute*time.Duration(cfg.SyncService.TimeForSync),
		dataBTC,
	)

	if err := service.LoadOldData(
		pair.InitPairStorage(txStore),
		candle.InitCandleStorage(txStore),
		dataBTC,
		models.BTC_USDT,
	); err != nil {
		logrus.Panic(err)
	}

	logrus.Info("Sync old data")

	time.Sleep(time.Second * 5)
	rsiService.ShowStatus()

	scheduler := service.InitScheduler()
	if _, err := scheduler.AddWorker(
		ctx,
		service.NewWorker("sync[BTC/USDT]", "*/6 * * * *", func() error {
			return syncService.SyncCandlePrice(
				ctx,
				models.BTC_USDT,
			)
		}),
	); err != nil {
		logrus.Panic(err)
	}

	wg := sync.WaitGroup{}

	wg.Add(1)
	go func() {
		defer wg.Done()
		if err := scheduler.Run(ctx); err != nil {
			logrus.Panic(err)
		}
	}()

	logrus.Info("Run service")

	<-stop

	logrus.Info("Stoping service...")
	cancel()

	close(dataBTC)
	close(rsiSubsribe)
	busService.Wait()
	rsiService.Wait()
}
