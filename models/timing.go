package models

type IntervalType string

const (
	FiveMinuts    = "5m"
	FifteenMinuts = "15m"
	ThirtyMinuts  = "30m"
	OneHour       = "1h"
	FourHours     = "4h"
	OneDay        = "1d"
)
