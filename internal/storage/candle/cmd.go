package candle

import (
	"context"
	"fmt"
	"indicator/models"
	"indicator/pkg/database"

	sq "github.com/Masterminds/squirrel"
)

func (u *CandleStorage) Create(ctx context.Context, candleList ...*models.CandlePriceUnit) (err error) {
	var (
		execFunc database.DBFunc
		args     []interface{}
		sql      string
	)

	if candleList == nil {
		return nil
	}

	// Query function function
	execFunc = func(conn database.Conn) (err error) {
		for _, candle := range candleList {

			// Gen query
			if sql, args, err = sqlInsertBuilder(candle); err != nil {
				err = fmt.Errorf("[Create] sqlInsertBuilder %w", err)
				return
			}

			er := conn.QueryRow(ctx, sql, args...).Scan(&candle.ID)
			if er != nil {
				return fmt.Errorf("[Create] scan candle data failed.\n\tErr: %w\n\tRSI: %s", err, candle.ToString())
			}

		}
		return nil
	}

	// Exec function
	if err = u.db.WithTx(ctx, execFunc); err != nil {
		err = fmt.Errorf("[Create] u.db.WithTx %w", err)
		return
	}

	return
}

func sqlInsertBuilder(candle *models.CandlePriceUnit) (string, []interface{}, error) {
	if candle == nil {
		return "", nil, nil
	}

	builder := sq.InsertBuilder{}.PlaceholderFormat(sq.Dollar).Into(models.TableCandles)
	sql, args, err := builder.Columns(
		"pair_id",
		"ts_open",
		"ts_close",
		"price_open",
		"price_close",
		"price_min",
		"price_max",
		"volumes",
		"quate",
		"number_of_trades",
		"taker_buy_base",
		"taker_buy_quote",
	).Values(
		candle.PairID,
		candle.TSOpen,
		candle.TSClose,
		candle.OpenPrice,
		candle.ClosePrice,
		candle.MinPrice,
		candle.MaxPrice,
		candle.Volume,
		candle.Quate,
		candle.NumberOfTrades,
		candle.TakerBuyBase,
		candle.TakerBuyQuote,
	).Suffix("ON CONFLICT (\"pair_id\", \"ts_open\", \"ts_close\") DO NOTHING RETURNING \"id\"").ToSql()

	if err != nil {
		return "", nil, fmt.Errorf("[sqlInsertBuilder] failed build insert query for rsi %w %s", err, candle.ToString())
	}

	return sql, args, nil
}
